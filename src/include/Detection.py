import cv2 as cv
import numpy as np


# def ComputeCornersDescriptors(img, corners, featureType = cv.SURF):
#     mask = np.zeros(img.shape)
#     for corner in corners:
#         mask[corner] = 1

#     descriptors = featureType.detectAndCompute(img, mask)
#     return descriptors

def DetectBoardCorners(img):
    gray_img = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

    block_corners = SegmentCorners(gray_img)

    cv.imshow('asd', block_corners)
    _, components = cv.connectedComponents(block_corners)

    corners = []
    for label in np.unique(components)[1:]:
        mask = np.uint8(np.where(components == label, 1, 0))
        dist = cv.distanceTransform(mask*block_corners, cv.DIST_L2, 3)
        x_max = np.argmax(np.max(dist, axis=0))
        y_max = np.argmax(np.max(dist, axis=1))
        corner = np.array([x_max, y_max])
        corners.append(corner)

    return len(corners), corners

def SegmentCorners(gray_img):
    gray_img = cv.GaussianBlur(gray_img, (5,5), 0)
    gray_img = cv.adaptiveThreshold(gray_img,255,cv.ADAPTIVE_THRESH_MEAN_C,\
            cv.THRESH_BINARY_INV,11,2)
    cv.imshow("Gray", gray_img)

    kernel = cv.getStructuringElement(cv.MORPH_CROSS, (30,30))
    block_corners = cv.morphologyEx(gray_img, cv.MORPH_ERODE, kernel, iterations= 1)
    cv.imshow('Erode com cross 70', block_corners)
    return block_corners

def OrderCorners(corners):
    # Getting array of the sum of coordinates.
    corners = np.array(corners)
    coord_sum = np.sum(corners, axis=1)

    # Top left corner has the smallest sum.
    tl_index = np.argmin(coord_sum)
    tl = corners[tl_index]
    corners = np.delete(corners, tl_index, axis=0)
    coord_sum = np.delete(coord_sum, tl_index, axis=0)

    # Bottom right corner has the greatest sum.
    br_index = np.argmax(coord_sum)
    br = corners[br_index]
    corners = np.delete(corners, br_index, axis=0)
    coord_sum = np.delete(coord_sum, br_index, axis=0)

    # Top right is the one with greater x coordinate from the remaining.
    print(corners)
    tr_index = np.argmax(corners[:,0])
    tr = corners[tr_index]
    corners = np.delete(corners, tr_index, axis=0)
    coord_sum = np.delete(coord_sum, tr_index, axis=0)

    # Bottom left is the one that remained.
    bl = corners[0]

    return tuple([tl,tr,bl,br])

def CornersAreSquare(corners, tolerance = 5):
    dists = []
    dists.append(np.linalg.norm(corners[1]-corners[0]))
    dists.append(np.linalg.norm(corners[1]-corners[3]))
    dists.append(np.linalg.norm(corners[3]-corners[2]))
    dists.append(np.linalg.norm(corners[2]-corners[0]))
    dist_std = np.std(np.array(dists))

    if dist_std > tolerance:
        return False
    else:
        return True

def GetBoardBB(corners):
    """
    Get bounding box of the tic-tac-toe board from inner corners.
    """
    inner_width = corners[1][0] - corners[0][0]
    inner_height = corners[2][1] - corners[0][1]

    xmin, ymin = corners[0] - np.array([inner_width, inner_height])

    bb = tuple([xmin, ymin, 3*inner_width, 3*inner_height])
    return bb

    