import cv2 as cv
import numpy as np
from include.Detection import DetectBoardCorners, CornersAreSquare, GetBoardBB, OrderCorners


wanted_num = 4
num_corners = -1
cap = cv.VideoCapture(0)
cv.namedWindow('main', cv.WINDOW_NORMAL)
tracker = cv.TrackerCSRT_create()
surf = cv.xfeatures2d.SURF_create()

while 1:
    ret, frame = cap.read()
    frame = cv.flip(frame, 1)

    key = cv.waitKey(1) & 0xFF

    # If board corners are not the number we want, detect again.
    if num_corners != wanted_num:
        num_corners, corners = DetectBoardCorners(frame)
        # If we found 4 corners, check if they form a square.
        if num_corners == wanted_num:
            # Restart number of corners if they do not form a square.
            if not CornersAreSquare(corners, tolerance=10):
                num_corners = -1
            else:
                corners = OrderCorners(corners)
                descriptors = surf.compute(frame, corners)
                print(descriptors)
                current_bb = GetBoardBB(corners)
                tracker.init(frame, current_bb)
    # If our four square corners were found, proceed with tracking.
    else:
        success, current_bb = tracker.update(frame)
        print(success)
        if success:
            # Plotting bounding box over frame.
            x, y, w, h = [int(v) for v in current_bb]
            cv.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # Check if bounding box is around board.
        x, y, w, h = [int(v) for v in current_bb]
        roi = frame[x:x+w,y:y+h]



    cv.imshow('main', frame)
    if key == ord('q'):
        break
